from django.contrib import admin
from .models import Product, Cart

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'description', 'price', 'quantity')

class CartAdmin(admin.ModelAdmin):
    list_display = ('userID', 'product_id', 'quantity')

admin.site.register(Product, ProductAdmin)
admin.site.register(Cart, CartAdmin)

