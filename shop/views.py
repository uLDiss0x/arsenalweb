from django.shortcuts import render
from .models import Product, Cart
from django.contrib.auth.models import User

def shop(request):
    products = Product.objects.all()
    return render(request, "shop/shop.html", {'products': products})

def product_detail(request, id):
    try:
        product = Product.objects.get(id=id)
    except Player.DoesNotExist:
        raise Http404('Product not found')
    return render(request, 'shop/product_detail.html', {'product': product})

def cart(request):
    try:
        userId = request.user.id
        cartOrders = Cart.objects.all().filter(userID=userId)
    except Cart.DoesNotExist:
        return render(request, "shop/cart.html", {'error': 'Your cart is empty'})

    totalSumPerProduct = []
    for productSum in cartOrders:
        totalSumPerProduct.append(productSum.quantity * productSum.product_id.price)
        
    mylist = zip(cartOrders, totalSumPerProduct)

    totalsum = 0

    for productPrize in totalSumPerProduct:
        totalsum += productPrize
        print(totalsum)

    return render(request, "shop/cart.html", {'mylist': mylist, 'totalsum' : totalsum})

def purchase(request):
    if request.method == 'POST':
        if request.POST['quantity']:

            usr = User(id = request.user.id)
            prod = Product.objects.get(id = request.POST['productId'])
            quan = request.POST['quantity']
            cart = Cart(userID=usr, product_id=prod, quantity=quan)
            cart.save()

            prod.quantity = prod.quantity - int(quan)
            prod.save()

            return render(request, "shop/purchase.html", {})

