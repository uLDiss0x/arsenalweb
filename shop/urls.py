from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

urlpatterns = [

    url(r'^$', views.shop, name='shop'),
    url(r'^product/(\d+)', views.product_detail, name='product_detail'),
    url(r'^cart', views.cart, name='cart'),
    url(r'purchase', views.purchase, name='purchase'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()

