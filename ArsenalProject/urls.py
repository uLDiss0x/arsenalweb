from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from arsenalapp import views

urlpatterns = [

    path('admin/', admin.site.urls),
    url(r'^$', views.home, name='home'),
    path('arsenalapp/', include('arsenalapp.urls')),
    path('accounts/', include('accounts.urls')),
    path('shop/', include('shop.urls')),
    path('api/', include('api.urls')),
    path('dashboard/', include('dashboard.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
