from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth

def register(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.get(username=request.POST['fanname'])
                return render(request, 'accounts/register.html', {'error': 'Fan name already exists!'})
            except User.DoesNotExist:
                user = User.objects.create_user(request.POST['fanname'], password=request.POST['password1'])
                auth.login(request, user)
                print("success");
                return redirect('home')
        else:
                return render(request, 'accounts/register.html', {'error': 'Passwords did not match!'})
    else:
        return render(request, "accounts/register.html", {})

def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['fanname'], password=request.POST['password'])
        
        if user is not None:
            auth.login(request, user)
            return redirect('home')
        else:
             return render(request, "accounts/login.html", {'error': 'Invalid username or password'})
    else:
        return render(request, "accounts/login.html", {})

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('home')
    else:
        return render(request, "accounts/logout.html", {})