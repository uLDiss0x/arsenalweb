from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.core.mail import send_mail
from .models import Player
from random import randint
from .forms import MailForm


def home(request):
    randomPlayer = randint(1, 10)
    playerToPass = Player.objects.get(id=randomPlayer)
    return render(request, "arsenalapp/home.html", {'player': playerToPass})

def player_detail(request, id):
    try:
        player = Player.objects.get(id=id)
    except Player.DoesNotExit:
        raise Http404('Player not found')
    return render(request, 'arsenalapp/player_detail.html', {'player': player})

def about(request):
    return render(request, "arsenalapp/about.html", {})

def team(request):
    players = Player.objects.all()
    return render(request, "arsenalapp/team.html", {'players': players})

def goalkeepers(request):
    players = Player.objects.all()
    return render(request, "arsenalapp/goalkeepers.html", {'players': players})

def defenders(request):
    players = Player.objects.all()
    return render(request, "arsenalapp/defenders.html", {'players': players})

def midfielders(request):
    players = Player.objects.all()
    return render(request, "arsenalapp/midfielders.html", {'players': players})

def strikers(request):
    players = Player.objects.all()
    return render(request, "arsenalapp/strikers.html", {'players': players})



def contact(request):

    if request.method == 'POST':
        form = MailForm(request.POST)
        if form.is_valid():
            yourMail = form.cleaned_data['yourMail']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']

            print(yourMail, subject, message)
            send_mail(subject, message, yourMail, ['uLDiss0x@abv.bg'])

    
    form = MailForm()
    return render(request, "arsenalapp/contact.html", {'form': form})