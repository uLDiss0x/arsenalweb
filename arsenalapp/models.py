from django.db import models

class Player(models.Model):
    POSITION_CHOICES = (
        ('goalkeeper', 'Goalkeeper'),
        ('defender', 'Defender'),
        ('midfielder', 'Midfielder'),
        ('striker', 'Striker'),
    )

    name = models.CharField(max_length = 50)
    position = models.CharField(max_length = 10, choices=POSITION_CHOICES)
    age = models.IntegerField()
    nationality = models.CharField(max_length = 25)
    description = models.TextField()
    picture = models.ImageField(upload_to = 'pictures/', default = 'pictures/DefaultAvatar.jpg')
    matchesPlayed = models.IntegerField()
    goalsScored = models.IntegerField()
    yellowCards = models.IntegerField()
    redCards = models.IntegerField()
    minutesPlayed = models.IntegerField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = "player"


    