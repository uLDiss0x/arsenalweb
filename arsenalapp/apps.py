from django.apps import AppConfig


class ArsenalappConfig(AppConfig):
    name = 'arsenalapp'
