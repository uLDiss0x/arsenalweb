from django.urls import path, include
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [

    url(r'^contact', views.contact, name='contact'),
    url(r'^about', views.about, name='about'),

    url(r'^team', views.team, name='team'),

    url(r'^goalkeepers', views.goalkeepers, name='goalkeepers'),
    url(r'^defenders', views.defenders, name='defenders'),
    url(r'^midfielders', views.midfielders, name='midfielders'),
    url(r'^strikers', views.strikers, name='strikers'),

    url(r'^players/(\d+)/', views.player_detail, name='player_detail'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
