from django import forms

class MailForm(forms.Form):
    yourMail = forms.EmailField(label='Your e-mail')
    subject = forms.CharField(label='subject', max_length=20)
    message = forms.CharField(widget=forms.Textarea)