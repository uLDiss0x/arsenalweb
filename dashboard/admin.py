from django.contrib import admin
from .models import Dashboard

class DashboardAdmin(admin.ModelAdmin):
    list_display = ('name', 'content', 'user', 'created', 'modified')

admin.site.register(Dashboard, DashboardAdmin)

