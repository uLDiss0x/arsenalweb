from django.db import models
from django.contrib.auth.models import User

class Dashboard(models.Model):
    POSITION_CHOICES = (
        ('accounts', 'Accounts'),
    )

    name = models.CharField(max_length=255)

    groups = models.CharField(max_length = 10, choices=POSITION_CHOICES, blank=True)

    content = models.TextField(null=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
