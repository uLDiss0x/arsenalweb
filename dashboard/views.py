from django.shortcuts import render
from .models import Dashboard
from django.contrib.auth.models import Group

def dashboard(request):
    try:
        userId = request.user.id
        yourDashboards = Dashboard.objects.all().filter(user=userId)
    except dashboards.DoesNotExist:
        return render(request, "dashboard/dashboard.html", {'error': 'Your dashboard is empty'})

    if request.user.groups.filter(name='Accounts').exists():
        accountGroupUsers = Group.objects.get(name="Accounts").user_set.all()
        for accounts in accountGroupUsers:
            # Get all dashboards that aren't yours
            if accounts.id != userId:
                accountDashboards = Dashboard.objects.all().filter(user_id=accounts.id).filter(groups='accounts')
                print(accounts)

                return render(request, "dashboard/dashboard.html", {'yourDashboards': yourDashboards, 'accountDashboards': accountDashboards})
    else:
        return render(request, "dashboard/dashboard.html", {'yourDashboards': yourDashboards})
