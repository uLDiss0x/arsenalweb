from django.shortcuts import render
from rest_framework import viewsets
from .serializers import PlayerSerializer
from arsenalapp.models import Player

class PlayerView(viewsets.ModelViewSet):
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer