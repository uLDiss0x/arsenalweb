from django.urls import path, include
from django.conf.urls import url
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'', views.PlayerView)

urlpatterns = [

    url(r'^', include(router.urls))

]
