from rest_framework import serializers
from arsenalapp.models import Player

class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Player
        fields = ('id', 'url', 'name', 'position', 'age', 'nationality', 'description', 'picture', 'matchesPlayed', 'goalsScored', 'yellowCards', 'redCards', 'minutesPlayed')
